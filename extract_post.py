import json

import pika
import requests
from utils import parameters

if __name__ == '__main__':
    headers = {
        'Content-Type': 'application/json',
    }
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='raw_post', durable=True)
    channel.queue_declare(queue='post_info', durable=True)
    channel.queue_declare(queue='list_page_id', durable=True)
    channel.queue_declare(queue='page_info', durable=True)
    channel.queue_declare(queue='final_page_info', durable=True)
    channel.queue_declare(queue='sdt', durable=True)
    f_data = '{ "size": 100, "query": { "match": { "docType": "page_post" } } , "sort": [ { "_index": { "order": "asc" } } ] , "_source": ["pageId","message","description"]}'
    response = requests.get('http://103.74.122.195:9200/dsminer_post_2021-08*/_search?scroll=2m', headers=headers,
                            data=f_data)
    first_resp = response.json()
    channel.basic_publish(exchange='', routing_key='raw_post', body=json.dumps(first_resp, ensure_ascii=False))
    scroll_id = first_resp.get('_scroll_id', None)
    total_post = first_resp.get('hits', {}).get('total', {}).get('value', None)
    s_data = '{ "scroll" : "2m", "scroll_id" :"' + scroll_id + '"}'
    for i in range(10000, total_post, 10000):
        loop_response = requests.get('http://103.74.122.195:9200/_search/scroll', headers=headers, data=s_data)
        json_resp = loop_response.json()
        channel.basic_publish(exchange='', routing_key='raw_post', body=json.dumps(json_resp, ensure_ascii=False))
        print("number line: " + str(i))
    end = "end_post"
    channel.basic_publish(exchange='', routing_key='raw_post', body=end)
    channel.close()
    connection.close()
