import json
import re
import time

import pika
import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


def calculate_revenue(shop_id):
    total_revenue = 0
    num_product = 0
    total_revenue_1m = 0
    headers_re = {'User-Agent': ua.random}
    product_links = None
    for offset in range(1, 50):
        total = 'https://www.lazada.vn/shop/site/api/shop/products/query?shopId=' + shop_id + '&offset=' + str(
            offset) + '&limit=300'
        r = requests.get(total, headers=headers_re)
        raw_data = r.json()
        product_info = raw_data.get('result', {}).get('data', None)
        num_product = raw_data.get('result', {}).get('totalCount', 0)
        if product_info:
            for product in product_info:
                price = product.get('price', 0)
                total_order = product.get('volumePayOrdPrdQtyStd', 0)
                order_1m = product.get('volumePayOrdPrdQty1m', 0)
                product_url = product.get('skus', {})[0].get('pdpUrl', None)
                if product_url:
                    product_links = product_url
                revenue = price * total_order
                revenue_1m = order_1m * price
                total_revenue_1m += revenue_1m
                total_revenue += revenue
        else:
            break
        time.sleep(1)
    return num_product, total_revenue, total_revenue_1m, product_links


def find_shop_name(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    all_script = soup.find_all('script')
    shop_name = None
    username = None
    for script in all_script:
        str_script = str(script)
        if 'var __moduleData__' in str_script:
            z = str_script.splitlines()
            for i in z:
                if 'var __moduleData__' in i:
                    t = i.replace('var __moduleData__ = ', '')
                    t = t[:-1]
                    a = json.loads(t)
                    seller = a.get('data', {}).get('root', {}).get('fields', {}).get('seller', {})
                    shop_name = seller.get('name', None)
                    link_pro = seller.get('url', None)
                    username = re.search(r'//www.lazada.vn/shop/([^/?]+)', link_pro).group(1)
    time.sleep(3)
    return shop_name, username


def find_shop_id(url):
    headers_shop = {'User-Agent': ua.random}
    r = requests.get(url, headers_shop)
    soup = BeautifulSoup(r.text, 'html.parser')
    scripts = soup.find_all('script')
    shop_id = None
    for row in scripts:
        script = str(row)
        try:
            a = script.split('shopId')[1]
            shop_id = re.search(r'\d+', a).group()
            break
        except:
            shop_id = None
    time.sleep(3)
    return shop_id


def check_lazada(link):
    z = None
    if link:
        if 'lazada' in link:
            z = 1
    return z


def combine_func(a):
    # a = json.loads(data)
    check = []
    page_id = a.get('Page ID', None)
    df = None
    for i in a['Links']:
        t = check_lazada(i)
        if t:
            s_id = find_shop_id(i)
            if s_id and s_id not in check:
                check.append(s_id)
                products, revenues, revenues_1m, links = calculate_revenue(s_id)
                if links:
                    name_shop, user_name = find_shop_name(links)
                else:
                    name_shop, user_name = None, None
                df = {'Page ID': page_id, 'Lazada ID': str(s_id), 'username': user_name, 'shop_name': name_shop,
                      'num_product': products,
                      'total_revenue': revenues, 'average_revenue': None, 'monthly_evenue': revenues_1m,
                      'years_has_joined': None}
    return df


if __name__ == '__main__':
    ua = UserAgent(use_cache_server=False)
    # credentials = pika.PlainCredentials('guest', 'guest')
    # parameters = pika.ConnectionParameters('localhost',
    #                                        5672,
    #                                        '/',
    #                                        credentials)
    # connection = pika.BlockingConnection(parameters)
    # channel = connection.channel()
    # channel.queue_declare(queue='lazada', durable=True)
    # print('Waiting data from queue')
    #
    #
    # def callback(ch, method, properties, body):
    #     print(" [x] Received %r" % body.decode())
    #     link = body.decode()
    #     lazada = combine_func(link)
    #     if lazada:
    #         with open('laz.json', 'a+', encoding='utf8') as f:
    #             f.write(json.dumps(lazada, ensure_ascii=False))
    #             f.write('\n')
    #     ch.basic_ack(delivery_tag=method.delivery_tag)
    #
    #
    # channel.basic_qos(prefetch_count=1)
    # channel.basic_consume(queue='lazada', on_message_callback=callback)
    # # channel.start_consuming()
    count = 0
    for line in open('data/output/final/final_page_info.json','r',encoding='utf-8'):
        count += 1
        print(count)
        a = json.loads(line)
        if a['Links']:
            lazada = combine_func(a)
            if lazada:
                    with open('laz.json', 'a+', encoding='utf8') as f:
                        f.write(json.dumps(lazada, ensure_ascii=False))
                        f.write('\n')