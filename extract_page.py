import itertools
import json

import requests

from utils import *


def create_command(list_page_id):
    numb = ",".join(map(str, list_page_id))
    data = '{"size": 10000,   "query":   {"bool":   { "should":   [{"terms":   {"pageId": [' + numb + ']}}]}},"_source": ["pageId","categories","name","link","about","website","numLikes","city","phone"]}'
    return data


def extract_page(resp):
    source = resp.get("_source", None)
    page_id = source.get('pageId', None)
    categories = source.get('categories', None)
    name = source.get('name', None)
    link = source.get('link', None)
    about = source.get('about', None)
    website = source.get('website', None)
    likes = source.get('numLikes', None)
    location = source.get('city', None)
    phone_source = remove_punc_number([source.get('phone', None)])
    phone_about, bank_account, bank, zalos, vibers, instas = run_all(about)
    d = list(set(itertools.chain(phone_source, phone_about, zalos, vibers)))
    phone = list(filter(None, d)) if len(list(filter(None, d))) > 0 else None
    zalo = zalos if len(zalos) > 0 else None
    viber = vibers if len(vibers) > 0 else None
    insta = None
    for j in instas:
        if j:
            insta = j
    links = find_links(about) if about else None
    page = {'Page ID': page_id, 'Name': name, 'Categories': categories, 'Location': location,
            'Link FB': link,
            'Website': website, 'Likes': likes,
            'Phone': phone, 'Links': links, 'Insta': insta, 'Zalo': zalo, 'Viber': viber,
            'STK': bank_account,
            'Bank': bank}
    return page


def start_pool(pool, raw_page_post):
    total_page = raw_page_post.get('hits', {}).get('hits', None)
    for page in pool.imap_unordered(extract_page, total_page):
        channel.basic_publish(exchange='', routing_key='page_info', body=json.dumps(page, ensure_ascii=False))


def send_request(line, index):
    list_data.append(line)
    if len(list_data) == 10000:
        query = create_command(list_data)
        response = requests.get('http://103.74.122.195:9200/facebook_page/_search', headers=headers, data=query)
        resp_json = response.json()
        start_pool(p, resp_json)
        list_data.clear()
    elif index == num_pages and len(list_data) > 0:
        query = create_command(list_data)
        response = requests.get('http://103.74.122.195:9200/facebook_page/_search', headers=headers, data=query)
        resp_json = response.json()
        start_pool(p, resp_json)


if __name__ == '__main__':
    count = 0
    list_data = []
    headers = {
        'Content-Type': 'application/json',
    }
    p = multiprocessing.Pool(cpu)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    all_post = []
    for method_frame, properties, body in channel.consume('list_page_id'):
        raw = body.decode()
        if raw == 'end_post':
            channel.basic_ack(method_frame.delivery_tag)
            break
        else:
            post_info = json.loads(raw)
            all_post.append(post_info)
            channel.basic_ack(method_frame.delivery_tag)
    all_keys = list(set(all_post))
    num_pages = len(all_keys)
    for i in all_keys:
        count += 1
        print(count)
        send_request(i, count)
    end = "end_post"
    channel.basic_publish(exchange='', routing_key='page_info', body=end)
    channel.close()
    connection.close()
