import json
import re
import time

import pika
import requests
from utils import parameters

def create_command(list_data):
    numb = '"' + '","'.join(list_data) + '"'
    data = '{ "size" : 10000 ,"query": {"bool": { "should": [{"terms": {"phone": [' + numb + ']}}]}},"_source": ["userId","name","phone","location","birthYear"]}'
    return data


def run(data):
    a = requests.get('http://103.74.122.195:9200/dsminer_user/_search', headers=headers, data=data)
    output = a.json()
    total_hits = output.get('hits', {}).get('hits', None)
    if total_hits is not None:
        with open('data/output/owner.json', 'a+', encoding='utf8') as f:
            for i in total_hits:
                z = i.get("_source", None)
                phone = z.get("phone", None)
                name = z.get("name", None)
                location = z.get("location", None)
                user_id = z.get("userId", None)
                birth = z.get("birthYear", None)
                if birth == '-1':
                    birth = None
                info = {'User Phone': str(phone), 'Owner Name': name, 'Birth Year': str(birth),
                        'User Location': location,
                        'User ID': str(user_id)}
                print(info)
                if info not in list_info:
                    list_info.append(info)
                    json.dump(info, f, ensure_ascii=False)
                    f.write('\n')
    time.sleep(0.5)


def check_latin(list_phone):
    new_list = []
    for i in list_phone:
        t = re.findall(r'[^\x00-\x7f]', i)
        if len(t) == 0:
            new_list.append(i)
    return new_list


def send_request(line, index):
    a = json.loads(line)
    phone_n = a.get('Phone', None)
    if phone_n:
        new_list = check_latin(phone_n)
        list_numb.extend(new_list)
    if len(list_numb) >= 10000:
        a = create_command(list(set(list_numb)))
        run(a)
        list_numb.clear()
    if index == num_lines and len(list_numb) > 0:
        a = create_command(list(set(list_numb)))
        run(a)


if __name__ == '__main__':
    count = 0
    list_numb = []
    list_page = []
    list_info = []
    headers = {
        'Content-Type': 'application/json',
    }
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    for method_frame, properties, body in channel.consume('sdt'):
        raw = body.decode()
        if raw == 'end_post':
            channel.basic_ack(method_frame.delivery_tag)
            break
        else:
            list_page.append(raw)
            channel.basic_ack(method_frame.delivery_tag)
    num_lines = len(list_page)
    for row in list_page:
        count += 1
        print(count)
        send_request(row, count)
